package com.example.umsspring.repository;

import com.example.umsspring.entity.JournalDataEntity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface JournalDataRepository extends JpaRepository<JournalDataEntity, Long> {
}
