package com.example.umsspring.service;

import com.example.umsspring.entity.AdminEntity;
import com.example.umsspring.entity.StudentEntity;
import com.example.umsspring.entity.TeacherEntity;

import java.util.List;

public interface AdminServices {


    void createAdmin();
    List<AdminEntity> getAdmins();


}
