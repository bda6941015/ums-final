package com.example.umsspring.service.impl;

import com.example.umsspring.entity.TeacherEntity;
import com.example.umsspring.repository.TeacherRepository;
import com.example.umsspring.service.TeacherServices;
import com.example.umsspring.service.UserServices;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Scanner;

@Service
public class TeacherServicesImpl implements TeacherServices {
    static Scanner scanner = new Scanner(System.in);

    @Autowired
    TeacherRepository teacherRepository;

    @Autowired
    UserServices userServices;

    public void createTeacher(){
        System.out.println("-------------------------");
        System.out.println("Muellim elave edilmesi");
        System.out.println("-------------------------");
        System.out.println("name:");
        String name = scanner.next();
        System.out.println("surname:");
        String surname = scanner.next();
        System.out.println("username:");
        String username = scanner.next();
        System.out.println("password:");
        String password = scanner.next();
        System.out.println("subject:");
        String subject = scanner.next();
        Long roleId = 2L;
        TeacherEntity teacher = new TeacherEntity(null,name,surname,username,password,subject,roleId);

        teacherRepository.save(teacher);
        userServices.createUser(name,surname,username,password,roleId);

    }
    public List<TeacherEntity> getTeachers(){
        return teacherRepository.findAll();
    }
}
