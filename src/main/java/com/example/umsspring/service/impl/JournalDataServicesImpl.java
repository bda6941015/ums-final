package com.example.umsspring.service.impl;


import com.example.umsspring.entity.*;
import com.example.umsspring.repository.*;
import com.example.umsspring.service.JournalDataServices;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;
import java.util.Scanner;

@Service
public class JournalDataServicesImpl implements JournalDataServices {
    static Scanner scanner = new Scanner(System.in);

    @Autowired
    JournalDataRepository journalDataRepository;

    @Autowired
    GroupRepository groupRepository;

    @Autowired
    JournalRepository journalRepository;

    @Autowired
    TeacherRepository teacherRepository;

    @Autowired
    StudentRepository studentRepository;

    public void createJournalData() {
        System.out.println("-------------------------");
        System.out.println("Jurnala datanin elave edilmesi");
        System.out.println("-------------------------");


        System.out.println("Jurnalin id-sini yazin");
        Long journalId = scanner.nextLong();
        Optional<JournalEntity> optionalJournal = journalRepository.findById(journalId);
        Long groupId = Long.parseLong(optionalJournal.stream().map(item -> item.getGroupEntity()).mapToLong(item -> item.getId()).toString());
        String groupName = optionalJournal.stream().map(item -> item.getGroupEntity()).map(item -> item.getName()).toString();

        GroupEntity group = new GroupEntity();
        group.setId(groupId);
        group.setName(groupName);

        JournalEntity journal = new JournalEntity();
        journal.setId(journalId);
        journal.setGroupEntity(group);

        System.out.println("Muellimin id-sini yazin:");
        Long teacherId = scanner.nextLong();
        Optional<TeacherEntity> optionalTeacher = teacherRepository.findById(teacherId);
        String name = optionalTeacher.stream().map(item -> item.getName()).toString();
        String surname = optionalTeacher.stream().map(item -> item.getSurname()).toString();
        String username = optionalTeacher.stream().map(item -> item.getUsername()).toString();
        String subject = optionalTeacher.stream().map(item -> item.getSubject()).toString();
        Long roleId = 2L;

        TeacherEntity teacher = new TeacherEntity(teacherId,name,surname,username,null,subject,roleId);

        System.out.println("Sagirdin id-sini daxil edin:");
        Long studentId = scanner.nextLong();
        Optional<StudentEntity> optionalStudent = studentRepository.findById(studentId);
        String studentName = optionalStudent.stream().map(item -> item.getName()).toString();
        String studentSurname = optionalStudent.stream().map(item -> item.getSurname()).toString();
        String studentUsername = optionalStudent.stream().map(item -> item.getUsername()).toString();
        Long studentRoleId = 3L;

        StudentEntity student = new StudentEntity(studentId,studentName,studentSurname,studentUsername,null,group,studentRoleId);

        String date = LocalDate.now().toString();

        System.out.println("Davamiyyet ( q / + ):");
        String attendance = scanner.next();

        JournalDataEntity journalData = new JournalDataEntity(null,group,journal,teacher,student,date,attendance);
        journalDataRepository.save(journalData);

    }

    public List<JournalDataEntity> getJournalDatas(){
        return journalDataRepository.findAll();
    }
}
