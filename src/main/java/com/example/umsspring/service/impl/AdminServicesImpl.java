package com.example.umsspring.service.impl;


import com.example.umsspring.entity.AdminEntity;
import com.example.umsspring.entity.StudentEntity;
import com.example.umsspring.entity.TeacherEntity;
import com.example.umsspring.repository.AdminRepository;
import com.example.umsspring.repository.StudentRepository;
import com.example.umsspring.repository.TeacherRepository;
import com.example.umsspring.repository.UserRepository;
import com.example.umsspring.service.AdminServices;
import com.example.umsspring.service.UserServices;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Scanner;

@Service
public class AdminServicesImpl implements AdminServices {
  static   Scanner scanner = new Scanner(System.in);

    @Autowired
    AdminRepository adminRepository;



    @Autowired
    UserRepository userRepository;

    @Autowired
    UserServices userServices;




    public void createAdmin(){
        System.out.println("-------------------------");
        System.out.println("Admin elave edilmesi");
        System.out.println("-------------------------");
        System.out.println("name:");
        String name = scanner.next();
        System.out.println("surname:");
        String surname = scanner.next();
        System.out.println("username:");
        String username = scanner.next();
        System.out.println("password:");
        String password = scanner.next();
        Long roleId = 1L;

        AdminEntity admin = new AdminEntity(null,name,surname,username,password,roleId);
        adminRepository.save(admin);
        userServices.createUser(name,surname,username,password,roleId);

    }

    public List<AdminEntity> getAdmins(){
        return adminRepository.findAll();
    }







}
