package com.example.umsspring.service;

import com.example.umsspring.entity.GroupDataEntity;

import java.util.List;

public interface GroupDataServices {
    void createGroupData();
    List<GroupDataEntity> getGroupDatas();
}
