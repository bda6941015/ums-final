package com.example.umsspring.controller;

import com.example.umsspring.entity.JournalDataEntity;
import com.example.umsspring.service.JournalDataServices;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequiredArgsConstructor
public class JournalDataController {
    @Autowired
    JournalDataServices journalDataServices;

    @GetMapping("/journal-data")
    public List<JournalDataEntity> getJournalDatas(){
      return journalDataServices.getJournalDatas();
    }

    @PostMapping("/add-attendance")
    public void addAttendance(){
        journalDataServices.createJournalData();
    }
}
