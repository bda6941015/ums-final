package com.example.umsspring.controller;


import com.example.umsspring.entity.GroupEntity;
import com.example.umsspring.service.GroupServices;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequiredArgsConstructor
//@RequestMapping("")
public class GroupController {

    @Autowired
    GroupServices groupServices;

    @GetMapping("/groups")
    public List<GroupEntity> getGroups(){
        return groupServices.getGroups();
    }


    @PostMapping("/admin/create-group")
    public void createGroup(){
        groupServices.createGroup();
    }
}
