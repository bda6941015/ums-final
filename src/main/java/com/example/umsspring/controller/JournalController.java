package com.example.umsspring.controller;

import com.example.umsspring.entity.JournalEntity;
import com.example.umsspring.service.JournalServices;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequiredArgsConstructor
public class JournalController {
    @Autowired
    JournalServices journalServices;

    @GetMapping("/journals")
    public List<JournalEntity> getJournals(){
        return journalServices.getJournals();
    }

    @PostMapping("/create-journal")
    public void createJournal(){
        journalServices.createJournal();
    }
}
