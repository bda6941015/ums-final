package com.example.umsspring.entity;

import lombok.*;
import lombok.experimental.FieldDefaults;

import javax.persistence.*;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Entity
@FieldDefaults(level = AccessLevel.PRIVATE)
@Table(name = "user")
public class UserEntity {

     @Id
     @GeneratedValue(strategy = GenerationType.SEQUENCE)
     Long id;

     @Column(name = "name")
     String name;

     @Column(name = "surname")
     String surname;

     @Column(name = "username")
     String username;

     @Column(name = "password")
     String password;

     @Column(name = "role_id")
     Long roleId;


}
